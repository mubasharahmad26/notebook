


import pandas as pd
import numpy as np


# ratings_list = [i.strip().split(";") for i in open('D:/Lastingsales/lastingmindEdges.csv', 'r').readlines()]
# # users_list = [i.strip().split(";") for i in open('D:/Lastingsales/users.dat', 'r').readlines()]
# movies_list = [i.strip().split(";") for i in open('D:/Lastingsales/lastingmind[Nodes].csv', 'r').readlines()]
x_cols = ['Source', 'Target','weight']
y_cols = ['ssid','UserID','label','modularity']
z_cols = ['Target','Target_label']
ratings_df = pd.read_csv('D:/Lastingsales/lastingmindEdges.csv', sep=';',header=0,names=x_cols, usecols=[0,1,6],
    encoding='latin-1',dtype='unicode')
users_df = pd.read_csv('D:/Lastingsales/new.csv', sep=',',header=0,names=y_cols, usecols=[0,1,2,3],
    encoding='latin-1',dtype='unicode')
movies_df = pd.read_csv('D:/Lastingsales/clients_with_ids.csv', sep=',',header=0,names=z_cols, usecols=[0,1],
    encoding='latin-1',dtype='unicode')
ratings_df.Source = pd.to_numeric(ratings_df.Source)
ratings_df.Target = pd.to_numeric(ratings_df.Target)
movies_df.Target = pd.to_numeric(movies_df.Target)
ratings_df.weight = pd.to_numeric(ratings_df.weight)
users_df.UserID = pd.to_numeric(users_df.UserID)
users_df.ssid = pd.to_numeric(users_df.ssid)
# users_df.ssid.astype(str).astype(int)

R_df = ratings_df.head().pivot_table(index = 'Source', columns ='Target', values = 'weight').fillna(0)
R = R_df.as_matrix()
user_ratings_mean = np.mean(R, axis = 1)
R_demeaned = R - user_ratings_mean.reshape(-1, 1)
from scipy.sparse.linalg import svds
U, sigma, Vt = svds(R_demeaned, k =2)
sigma = np.diag(sigma)
all_user_predicted_ratings = np.dot(np.dot(U, sigma), Vt) + user_ratings_mean.reshape(-1, 1)
preds_df = pd.DataFrame(all_user_predicted_ratings, columns = R_df.columns)
    # preds_df


def recommend_movies(userID,num_recommendations=5):

    checker = 0;
    user = str(userID)
    import csv
    with open('D:/Lastingsales/new.csv', 'rt') as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            if row[1] == user:
                user = int(row[0])
                checker=1;
                break;

    if checker==0:
        return add_new(userID,users_df,num_recommendations)


    # Get and sort the user's predictions
    user_row_number = user # UserID starts at 1, not 0
    sorted_user_predictions = preds_df.loc[user_row_number]

    # Get the user's data and merge in the movie information.
    user_data = ratings_df[ratings_df.Source == (userID)]
    user_full = (user_data.merge(movies_df, how = 'left', left_on = 'Target', right_on = 'Target').
                     sort_values(['weight'], ascending=False)
                 )

    # Recommend the highest predicted rating movies that the user hasn't seen yet.
    recommendations = (movies_df[~movies_df['Target'].isin(user_full['Target'])].
         merge(pd.DataFrame(sorted_user_predictions).reset_index(), how = 'left',
               left_on = 'Target',
               right_on = 'Target').
         rename(columns = {user_row_number: 'Predictions'}).
         sort_values('Predictions', ascending = False).
                       iloc[:num_recommendations, :-1]
                      )

    return recommendations

def add_new(userID,users_df,num_recommendations):
    import csv
    with open('D:/Lastingsales/new.csv','a') as f:
            writer=csv.writer(f)
            writer.writerow([users_df.ssid.max()+1,userID,userID,0])
    return recommend_movies(userID,num_recommendations)



from flask import Flask  # From 'flask' module import 'Flask' class
app = Flask(__name__)    # Construct an instance of Flask class for our webapp
app.debug = True
@app.route('/')   # URL '/' to be handled by main() route handler (or view function)
def main():
    return 'Hello'

@app.route('/<page_id>')   # URL '/' to be handled by main() route handler (or view function)
def id(page_id):

    ids=int(page_id)
    predictions = recommend_movies(ids)
    json_obj = predictions.to_json()
    
    # json_obj = predictions.to_json(orient='values')[1:-1].replace('},{', ',')
    ids = str(predictions)
    exam = '''<html>
        <head>Suggestions</head>
        <body>
        <header
        <p>Hello, 'world'!</p>
        </body>
        </html>'''
    # json_obj = json_obj.replace(',"',' ')
    # json_obj = json_obj.replace('"','')
    # json_obj = json_obj.replace('[','')
    # json_obj = json_obj.replace(']','')
    # json_obj = json_obj.replace(',','\n')
    return json_obj

if __name__ == '__main__':  # Script executed directly (instead of via import)?
    app.run(host='127.0.4.1', port=80)
